from django.conf.urls import patterns, include, url

from brochure import views as bviews

urlpatterns = patterns('',
                       url(r'^$', bviews.index, name='bindex')
                       )
