# (Geo) Django specific imports

from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',

    url(r'', include('brochure.urls')),
    url(r'^viewer/', include('viewer.urls')),
    url(r'^admin/', include(admin.site.urls))


)
