# (Geo)Django level imports

from django.contrib.gis import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.gis.geos import GEOSGeometry

# Project/App level imports

from models import Bug, Visualisation

class BugForm(forms.ModelForm) :

    # inline class to provide additional information on the form
    class Meta:
        model = Bug
        fields = ('trackingNumber',
                  'headline',
                  'notes',
                  'url'
                  )
        labels = {
            'trackingNumber': _('Tracking No.'),
            'headline': _('Headline'),
            'notes': _('Notes'),
            'url': _('URL'),
        }

class VisualizationForm(forms.ModelForm):

   #inline class to provide additional information on the form

   def clean(self):

    """
    This is meant to over-ride the default ModelForm cleaning method to check for specific fields' validity. It receives the output of the normal cleaned data and uses them for further checks.
    """
    cleaned_data = self.cleaned_data
    geom_field = cleaned_data.get('geom')

    #Validation 1: Is this a valid geometry value?

    if geom_field is not None and GEOSGeometry(geom_field):

        pass

    else:

        raise forms.ValidationError("The value for the geometry field must be valid EWKT, WKB or GeoJSON")

    return cleaned_data

    class Meta:

        model = Visualisation
        fields = ('bug',
                  'notes',
                  'geom'
                  )

        labels = {

            'bug' : _('Associated Bug'),
            'headline' : _('Headline'),
            'geom' : _('Geometry field')
        }

        widgets = {
            'geom': forms.Textarea
        }



