from django.contrib.gis import admin
from models import Bug, Visualisation

# Register your models here.

admin.site.register(Bug, admin.GeoModelAdmin)
admin.site.register(Visualisation, admin.GeoModelAdmin)
