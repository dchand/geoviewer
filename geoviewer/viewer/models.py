# imports

from django.contrib.gis.db import models

# models

class Bug(models.Model):

    trackingNumber = models.CharField('Tracking Number', max_length=100, unique = True)
    headline = models.CharField('Bug Headline', max_length=200, unique = False)
    notes = models.TextField('Bug Notes')
    last_changed = models.DateTimeField()
    url = models.URLField(null = True, blank= True)

    # Return string representation of self for debugging

    def __unicode__(self):
        return u'%s' % (self.trackingNumber)


class Visualisation(models.Model):

    # Bug
    bug = models.ForeignKey(Bug)
    last_changed = models.DateTimeField()
    notes = models.TextField('Bug Notes')
    geom = models.GeometryField()

    #Objects manager

    objects = models.GeoManager()

    # Return string representation of self for debugging

    def __unicode__(self):
        return u'%s' % (self.trackingNumber)



