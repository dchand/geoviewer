#(Geo)Django level imports

from django.shortcuts import render

# My project specific imports

from viewer.forms import BugForm, VisualizationForm
from viewer.models import Bug, Visualisation


def index(request):

    return render(request,
                  'index.html')

def detail_bug(request, bug_id):

    bug = get_object_or_404(Bug, pk=bug_id)
    return render(request,
                  'viewer/bug_detail.html',
                  {
                      'bug' : bug
                  }
                  )

def add_bug(request):

    # Is this HTTP POST?
    if request.method == 'POST':
        form = BugForm(request.POST)

        # check for valid form

        if form.is_valid():
            # add the new Bug to the database
            form.save(commit=True)

            # once the form is saved, redirect to homepage
            return index(request)

        else:

            # this form has errors - log them
            print form.errors

    # If it's not POST, then allow user to enter form details for new Bug

    else:

        form = BugForm()

    # if neither of the above two obtained then we have a problem

    return render(request,
                   'add_bug.html',
                  {
                       'form': form
                   }

                  )

def detail_visualisation(request, visualisation_id):

    visualisation = get_object_or_404(
        Visualisation,
        pk=visualisation_id
    )

    return render(request,
                  'viewer/detail_visualisation.html',
                  {
                      'visualisation' : visualisation
                  }
                  )

def add_visualisation(request):

    #Is this HTTP POST?

    if request.method == 'POST':
        form = VisualisationForm(request)

        # check for valid form

        if form.is_valid():
            form.save(commit=True)

            #upon save redirect to homepage
            return index(request)

        else:

            # this form has errors - log them
            print form.errors

    # If it's not POST then allow user to enter form details for new Visualisation

    else:

        form = VisualizationForm()

    # and if neither of those are met then Houston, we have a problem

    return render(request,
                  'add_visualisation.html',
                  {
                    'form' : form
                  }
                  )




