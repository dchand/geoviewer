# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Bug',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('trackingNumber', models.CharField(unique=True, max_length=100, verbose_name=b'Tracking Number')),
                ('headline', models.CharField(max_length=200, verbose_name=b'Bug Headline')),
                ('notes', models.TextField(verbose_name=b'Bug Notes')),
                ('last_changed', models.DateTimeField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Visualisation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('last_changed', models.DateTimeField()),
                ('notes', models.TextField(verbose_name=b'Bug Notes')),
                ('geom', django.contrib.gis.db.models.fields.GeometryField(srid=4326)),
                ('bug', models.ForeignKey(to='viewer.Bug')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
