from django.conf.urls import patterns, include, url

from viewer import views
urlpatterns = patterns('',
                       # Ex: /viewer/
                       url(r'^$', views.index, name='index'),
                       # Ex: /bug/add/
                       url(r'bug/add/', views.add_bug, name='add_bug'),
                       #Ex: /bug/CISCO912-23
                       url(r'^bug/([a-zA-Z0-9_-]+/)$', views.detail_bug, name='detail_bug'),
                       #Ex: /visualisation/add/
                       url(r'^visualisation/add', views.add_visualisation, name='add_visualisation'),
                       #Ex: /visualisation/SFTLYR898_A8/
                       url(r'^visualisation/add', views.detail_visualisation, name='detail_visualisation'),
                       )


