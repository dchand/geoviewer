#Welcome To The GeoViewer

This is a project written by Dheeraj Chand. It is based on my [GeoDjango Template](https://github.com/dheerajchand/ubuntu-django-nginx-ansible).

The simple goal is to take text-formats of spatial data and allow you to visualise them, while preserving an association of the visualisation with an originating bug from your bug tracking system.
Each bug can have multiple visualisations associated with it.

All you have to do is create a Visualisation from :

1. WKT
2. WKB
3. GeoJSON

and the application will not only create all other missing formats for you, but will also show you visually what it looks like.

##Host Environment

###Install VirtualBox
https://www.virtualbox.org/wiki/Downloads

###Install Vagrant
https://www.vagrantup.com/downloads.html

###Install Ansible
http://docs.ansible.com/intro_installation.html

##Development Environment
Make sure git doesn't change line endings:
```
git config --global core.autocrlf input
```

##Startup
Start VM with Vagrant. This will use Ansible to install dependencies and run a script to set up virtual environment and pip requirements.
```
vagrant up --provision
```
Open browser to http://localhost:8080 to check that everything works. It should redirect you to an information page.

You will also need to create your own Django superuser for the project. You can do that by:

```
vagrant ssh
sudo su
cd /opt/venv/$projectname
source /opt/venv/bin/activate
python manage.py createsuperuser
```

or by using the manage.py tool in your IDE. [PyCharm](https://www.jetbrains.com/pycharm/) has that built in!

This project comes with GeoServer installed, but it is currently not used. To configure your GeoServer, you will need to go to [http://localhost:8081/geoserver](http://localhost:8081/geoserver) and log in. The default credentials are *admin* , *geoserver*.  From there, you can do whatever you need. 

##Management
If you make any changes to Vagrantfile, requirements.txt, or default.pp:
```
vagrant reload --provision
```
If you need to shut down or reboot your laptop, or just want to stop the VM:
```
vagrant halt
```
To log onto VM:
```
vagrant ssh
```
To get rid of a VM if you are done or it was corrupted:
```
vagrant destroy
```
If you a change is made to Vagrantfile or requirements.txt, do
```
vagrant reload -–provision
```